#!/bin/bash

apt-get -y install openvpn
ID=$1
scp root@vps169292.ovh.net:/etc/openvpn/easy-rsa/keys/\{$ID.key,$ID.crt,ca.crt,../../client.conf\} /etc/openvpn
sed -i "s/^cert\s*.*$/cert $ID.crt/" /etc/openvpn/client.conf
sed -i "s/^key\s*.*$/key $ID.key/" /etc/openvpn/client.conf
service openvpn restart