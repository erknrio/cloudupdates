begin
  gem "dropbox-sdk"
rescue LoadError
  system("gem install dropbox-sdk")
  Gem.clear_paths
end


require 'dropbox_sdk'

module Script_Utils
  module Dropbox

    OAUTH2 = "IXxmLXplpQIAAAAAAAAAAcR9BxIW0C6gwWbOVaUb-S2dY3w1w2_ZUxMYBOB7vUZr"
    @client = DropboxClient.new(OAUTH2)
    
    def self.upload(filename, folder = "") 
      file = open(filename)
      response = @client.put_file("#{folder}/#{File.basename(filename)}", file, true)
      puts response.inspect
    end 
  end 
end

