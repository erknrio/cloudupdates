require 'net/http'
require 'uri'
require 'logger'
require 'usagewatch'
require 'json'
require 'cpu'

def send_info(data)
    uri = URI.parse("http://178.79.152.44:8091/")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri,initheader = {'Content-Type' =>'application/json'})
    request.body = data
    puts http.inspect
    response = http.request(request)
    response.body
end

def empty_output(cmd)
  if cmd.empty?
    2
  else 
    0
  end
end

def perc(cmd)
  if cmd.is_a? Numeric 
    val = cmd
  else
    m = /(\d{1,3})%/.match(cmd)
    if m
      val = m[1].to_i
    else 
      val = -1
    end
  end
    if val < 60
      0
    elsif val < 90
      1
    else
      2
    end
end

def non_zero(cmd)
  if cmd != '0'
    0
  else
    2
  end
end

usw = Usagewatch
cpu = CPU

datos = {}

services = {
                  :disk_used   => {
                                :cmd   => `df -Ph | grep /$`.strip!,
                                :check => 'perc'
                                  },
                  :puerto_4004 => {
                                :cmd   => `netstat -anp | grep :4004`,
                                :check => 'empty_output'
                                  },

                  :chromium    => {
                                :cmd   => `ps aux | grep chromium | wc -l`,
                                :check => 'non_zero'
                                 },
                  :ram         => {
                                :cmd => usw.uw_memused * 100.0,
                                :check => 'perc'
                                  },
                  :resolucion  => {
                                 :cmd => `xrandr | grep connected | awk '{ print $3, "(",$4, "x", $6,")" }'`,
                                 :check => 'non_zero'
                                  },
                  :ssh         => {
                                 :cmd => `netstat -anp | grep :22`,
                                 :check => 'empty_output'
                                  },
                  :cpu         => {
   		                 :cmd => usw.uw_cpuused,
                		 :check => 'perc'
                         	  }
           }

puts services
datos[:id_Dispositivo] = ARGV[0] #id del dispositivo
datos[:tiempo_Encendido] = `uptime`.strip #Tiempo que lleva encendido
datos[:serv] = { }
services.each { |srv, val|
  datos[:serv][srv] = {
                        :message => val[:cmd],
                        :status  => send(val[:check], val[:cmd])
                      }
}

puts datos.to_json
puts send_info(datos.to_json)
